import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import Cookies from "js-cookie";
import { type } from "os";
import util from "@/util.js";
import { EventBus } from "../event-bus.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentUserDetails: {},
    states: [],
    professionalDesignations: [],
    brokerageList: [],

    location_names: [],
    brokerage_names: [],
    brokerData: {},
    messages: [],
    current_chat_room: null,
    current_question_id: null,
    requestList: null,
    res: null,
    chats: {},
    chatMessages: {},
    currentChat: "",
    friends: [],
    me: {},
    chat_agent_id: null,
    chat_broker_id: null,
    chat_question_id: null,
    chatHistory: [],
    leadDataDetailsSingle: {},
    currentBrokerId: null,
    brokerPercentage: null,
    subMemberDetails: [],
    connected_details: [],
    currentBrokersQuestionsId: null,
    OtherUserId: null,
    companyList: [],
    ques_no: 0,
    globalQuestionNo: 0
  },
  plugins: [
    createPersistedState({
      object: {},
      paths: [
        "currentUserDetails",
        "states",
        "brokerage_names",
        "location_names",
        "professionalDesignations",
        "brokerageList",
        "current_question_id",
        "res",
        "connected_details",
        "currentBrokerId",
        "currentBrokersQuestionsId",
        "OtherUserId",
        "subMemberDetails",
        "companyList"
      ],
      getItem: key => Cookies.get(key),
      setItem: (key, value) =>
        Cookies.set(key, value, { expires: 3, secure: true }),
      removeItem: key => Cookies.remove(key)
    })
  ],

  mutations: {
    updateQuesNo(state, no) {
      state.ques_no = no;
    },
    updateGlobalQuesNo(state, no) {
      state.globalQuestionNo = no;
    },
    setCurrentBroker(state, broker_id) {
      state.currentBrokerId = broker_id;
    },
    SetCurrentOtherUserId(state, user_id) {
      state.OtherUserId = user_id;
    },
    setCurrentQuestionId(state, question_id) {
      state.currentBrokersQuestionsId = question_id;
    },
    ConnectedStatus(state, payload) {
      state.connected_details = payload;
    },
    currentUserDetailsAdd(state, payload) {
      state.currentUserDetails = payload;
    },
    currentBrokerPercentage(state, payload) {
      state.brokerPercentage = payload;
    },
    GET_STATES(state, states) {
      state.states = states;
    },
    GET_PROFESSIONAL_DESIGNATION(state, pro) {
      state.professionalDesignations = pro;
    },
    GET_BROKERAGE_LIST(state, brokerage) {
      state.brokerageList = brokerage;
    },
    GET_LOCATION_NAMES(state, location) {
      state.location_names = location;
    },
    GET_BROKERAGES_NAMES(state, brokerages) {
      state.brokerage_names = brokerages;
    },
    GET_COMPANY_NAMES(state, brokerages) {
      state.companyList = brokerages;
    },
    putBrokerData(state, payload) {
      state.brokerData = payload;
    },
    pushMessages(state, payload) {
      state.messages.push(payload);
    },
    changeChatRoom(state, payload) {
      state.current_chat_room = payload;
    },
    changeQuestionId(state, payload) {
      state.current_question_id = payload.id;
    },
    requestList(state, payload) {
      state.requestList = payload;
    },
    selected_interest_data(state, payload) {
      state.res = payload;
      // state.res.interest = payload.data.interest
      // state.res.renewal_date = payload.data.renewal_date
    },
    chatIdSet(state, payload) {
      state.chat_agent_id = payload.agent_id;
      state.chat_broker_id = payload.broker_id;
      state.chat_question_id = payload.question_id;
    },
    pushMessagesHistory(state, payload) {
      state.chatHistory.push(payload);
      // console.log("typeof",typeof(state.chatHistory))
    },
    setMe(state, { me }) {
      state.me = me;
    },
    setCurrentChat(state, { chatKey }) {
      state.currentChat = chatKey;
    },
    setFriends(state, { friends }) {
      for (let friend of friends) {
        state.friends.push(friend);
      }
    },
    newChat(state, { chat }) {
      if (!chat.key) {
        throw Error("No chat.key defined on the new Chatengine chat Object");
      }
      state.chats[chat.key] = chat;
    },
    CHATENGINE_message(state, { event, sender, chat, data, timetoken }) {
      let key = chat.key || chat.chat.key;

      if (!state.chatMessages[key]) {
        Vue.set(state.chatMessages, key, []);
      }

      let message = data;
      message.who = sender.uuid;
      message.time = new Date(timetoken / 1e4); // timetoken in ChatEngine 0.9.5 or later

      // Force stop the typing indicator
      if (chat.typingIndicator && sender.name !== "Me") {
        // Handler in Chat Log Component (components/ChatLog.vue)
        EventBus.$emit("typing-stop", chat.key);
      }

      state.chatMessages[key].push(message);
      state.chatMessages[key].sort((msg1, msg2) => {
        return msg1.time > msg2.time;
      });
    },
    leadDataDetails(state, details) {
      state.leadDataDetailsSingle = details;
    },
    setSubMemberDetails(state, details) {
      state.subMemberDetails = details;
    }
  },
  actions: {
    getLocations({ commit }) {
      Vue.http
        .get(process.env.VUE_BACKEND_URL + "location-dropdown")
        .then(res => {
          var options = [];
          commit("GET_STATES", res.body.data);
          for (var i = 0; i < res.body.data.length; i++) {
            options.push(res.body.data[i].location_name);
          }
          commit("GET_LOCATION_NAMES", options);
          // self.getStates();
        })
        .catch(err => {
          console.log(err);
        });
    },
    getProfessionalDesignation({ commit }) {
      Vue.http
        .get(process.env.VUE_BACKEND_URL + "professional-designation-dropdown")
        .then(res => {
          commit("GET_PROFESSIONAL_DESIGNATION", res.body.data);
          // self.getProfessionalDesignations();
        })
        .catch(err => {
          console.log(err);
        });
    },
    getBrokerageList({ commit }) {
      Vue.http
        .get(process.env.VUE_BACKEND_URL + "brokerage-dropdown-selected")
        .then(res => {
          // commit("GET_BROKERAGE_LIST", res.body.data);
          var options = [];
          for (var i = 0; i < res.body.data.length; i++) {
            options.push(res.body.data[i].brokerages);
          }
          commit("GET_BROKERAGE_LIST", options);
          // self.getBrokerageLists();
        })
        .catch(err => {
          console.log(err);
        });
    },
    getCompanyList({ commit }) {
      Vue.http
        .get(process.env.VUE_BACKEND_URL + "company-dropdown-selected")
        .then(res => {
          // commit("GET_COMPANY_LIST", res.body.data);
          var options = [];
          for (var i = 0; i < res.body.data.length; i++) {
            options.push(res.body.data[i].companies);
          }
          commit("GET_COMPANY_NAMES", options);
          // self.getBrokerageLists();
        })
        .catch(err => {
          console.log(err);
        });
    },
    sendMessage(context, { chat, message }) {
      // emit the `message` event to everyone in the Chat
      console.log("actions", chat, message);
      context.state.chats[chat].emit("message", message);
    }
  },
  getters: {
    getMyUuid: state => state.me.uuid,
    getFriends: state => state.friends
  }
});
