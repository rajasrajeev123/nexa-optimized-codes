import typingIndicator from 'chat-engine-typing-indicator';
import ChatEngineCore from 'chat-engine';
import {EventBus} from './event-bus.js';
const status = require('chat-engine-event-status')
const unread_message = require('chat-engine-unread-messages')

const desktopNotifications = require('chat-engine-desktop-notifications');
/**
 * Makes a new, private ChatEngine Chat and adds it to the global Vuex store.
 *
 * @param {Object} store Global Vuex store object.
 * @param {Object} chatEngine ChatEngine client.
 * @param {Object} friend Friend settings like avatar, chatKey, name.
 * @param {Boolean} private_ True will make the Chat a private Chat.
 *
 * @return {Object} Chat object that was initialized and added to the store.
 */
function newChatEngineChat(store, chatEngine, friend, private_) {
  // Make a new 1:1 private chat.
  const newChat = new chatEngine.Chat(friend.chatKey, private_,false);

  newChat.on('$.connected', () => {   // add listener

    newChat.search({
          event: 'message',
          reverse:false
      })
    });
    
    newChat.connect() // Now connect to chat
  
  // Add the key to the Chat object for Vue UI use
  newChat.key = friend.chatKey;

  // console.log("newChat",newChat)
  // Add the Typing Indicator ChatEngine plugin to private 1:1 chats
  if (private_) {
    _addTypingIndicator(newChat);
    _currentMessageStatus(newChat);
  }
  
  // If there is no name, make one with the UUID
  if (!friend.name) {
    friend.name = `Friend: ${friend.uuid}`;
  }

  // Add this friend to the client's friend list
  store.commit('setFriends', {
    friends: [friend],
  });

  // Add this chat to the global state
  store.commit('newChat', {
    chat: newChat,
  });

  return newChat;
}

/**
 * Adds the ChatEngine Typing indicator plugin and initializes the events
 *     that update the UI via the Vue Event Bus.
 *
 * @param {Object} chat Chat object to add the typing indicator to.
 */
function _addTypingIndicator(chat) {
  chat.plugin(typingIndicator({
    timeout: 2000, // MS after final keyup when stopTyping fires
  }));

  chat.on('$typingIndicator.startTyping', (event) => {
    const chat = event.chat;
    const me = event.sender.name === 'Me' ? true : false;

    // Only fire the UI changing event if the sender is not Me
    if (!me) {
      // Handler in Chat Log Component (components/ChatLog.vue)
      EventBus.$emit('typing-start', chat.key);
    }
  });

  chat.on('$typingIndicator.stopTyping', (event) => {
    const chat = event.chat;
    const me = event.sender.name === 'Me' ? true : false;

    // Only fire the UI changing event if the sender is not Me
    if (me) {
      // Handler in Chat Log Component (components/ChatLog.vue)
      EventBus.$emit('typing-stop', chat.key);
    }
  });
}
/** 
 * @param {Object} chat 
*/

function _currentMessageStatus(chat){
  chat.plugin(status({
    event: 'message' // MS after final keyup when stopTyping fires
  }));
}

/**
 * Get a new 4 character ID. This is used in the ChatEngine User configuration
 *     as the 'uuid' property. It is recommended to use a standard 128-bit UUID
 *     in production apps instead.
 *
 * @return {string} A unique ID for ChatEngine, and to give to friends.
 */
function fourCharID() {
  const maxLength = 4;
  const possible = 'abcdef0123456789';
  let text = '';

  for (let i = 0; i < maxLength; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

/**
 * Make an HTTP POST request wrapped in an ES6 Promise.
 *
 * @param {String} url URL of the resource that is being requested.
 * @param {Object} options JSON Object with HTTP request options, "header"
 *     Object of possible headers to set, and a body Object of a POST body.
 *
 * @return {Promise} Resolves a parsed JSON Object or String response text if
 *     the response code is in the 200 range. Rejects with responce status text
 *     when the response code is outside of the 200 range.
 */
function post(url, options) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();

    xhr.open('POST', url);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    for (let header in options.headers) {
      if ({}.hasOwnProperty.call(options.headers, header)) {
        xhr.setRequestHeader(header, options.headers[header]);
      }
    }

    xhr.onload = function() {
      if (xhr.status >= 200 && xhr.status < 300 ) {
        let response;

        try {
          response = JSON.parse(xhr.response);
        } catch (e) {
          response = xhr.response;
        }

        resolve(response);
      } else {
        reject(xhr.statusText);
      }
    };

    xhr.send(JSON.stringify(options.body));
  });
}


function unreadMessagesInActiveAgent(chat) {
  chat.plugin(unread_message()); 
  chat.unreadMessages.inactive();
  chat.on('$unread', (payload) => {
       if(chat.unreadCount!=0){
        var active = $('.chat-listing').find('.active')
        $('#chatMessageCountAgent'+active.attr('id')).removeClass('d-none');
        $('#chatMessageCountAgent'+active.attr('id')).html(chat.unreadCount);
      }
  });
}


function unreadMessagesInActiveBroker(chat) {
  chat.plugin(unread_message()); 
  chat.unreadMessages.inactive();
  chat.on('$unread', (payload) => {
    if(chat.unreadCount!=0){
      $('#chatMessageCountBroker').removeClass('d-none');
      $('#chatMessageCountBroker').html(chat.unreadCount);
    }
  });
}


function unreadMessagesActiveAgent(chat) {
  chat.plugin(unread_message()); 
  chat.unreadMessages.active();
    var active = $('.chat-listing').find('.active');
    $('#chatMessageCountAgent'+active.attr('id')).html(0);
    $('#chatMessageCountAgent'+active.attr('id')).addClass('d-none');
}


function unreadMessagesActiveBroker(chat) {
  chat.plugin(unread_message()); 
  chat.unreadMessages.active();
  $('#chatMessageCountBroker').html(0);
  $('#chatMessageCountBroker').addClass('d-none');
}


function findUnreadMessageAgent(chat,id){
  chat.plugin(unread_message());
    if(chat.unreadCount!=0){
      // var active = $('.chat-listing').find('.active')
      $('#chatMessageCountAgent'+id).removeClass('d-none');
      $('#chatMessageCountAgent'+id).html(chat.unreadCount);
    }
}


function findUnreadMessageBroker(chat,id){
  chat.plugin(unread_message());
    $('#chatMessageCountBroker').removeClass('d-none');
    $('#chatMessageCountBroker').html(chat.unreadCount);

}

function DesktopNotifications(chat){
  chat.plugin(desktopNotifications({
    title: (event) => {
        console.log(event)
        return 'Message In ' + room.name
    },
    message: (event) => {
      console.log(event)
        return event.data.message;
    },
    icon: (event) => {
      console.log(event)
        return '/examples/flowtron/logo@2x.png';
    },
    callback: (event) => {
      console.log(event)
        window.focus();
    }
}));
    
}

export default {
  newChatEngineChat,
  unreadMessagesActiveBroker,
  unreadMessagesActiveAgent,
  unreadMessagesInActiveBroker,
  unreadMessagesInActiveAgent,
  findUnreadMessageAgent,
  findUnreadMessageBroker,
  fourCharID,
  DesktopNotifications,
  post,
};
