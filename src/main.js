/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import router from "@/router";
import NexaMainPage from "@/NexaMainPage";
import Vueresource from "vue-resource";
import VeeValidate from "vee-validate";
import VueSession from "vue-session";
import vSelect from "vue-select";
import Loading from "vue-loading-overlay";
import store from "@/store";
import VueChatEngine from "vue-chat-engine";
import ChatEngineCore from "chat-engine";
import PubNubVue from "pubnub-vue";
// import axios from 'axios'
// import VueAxios from 'vue-axios'
import VueChatScroll from "vue-chat-scroll";
import Vuesax from "vuesax";
import "material-icons/iconfont/material-icons.css";
import VueElementLoading from "vue-element-loading";
import "font-awesome/css/font-awesome.css";
import Pusher from "pusher-js";
import VueWorker from "vue-worker";
import Tooltip from "vue-directive-tooltip";
import "vue-directive-tooltip/dist/vueDirectiveTooltip.css";
import VueIntro from "vue-introjs";
import "intro.js/introjs.css";
var VueScrollTo = require("vue-scrollto");
const introJs = require("intro.js");

global.introJs = introJs;

export const bus = new Vue();
import VueCookies from "vue-cookies";
import VueSweetalert2 from "vue-sweetalert2";
import VueMoment from "vue-moment";
import moment from "moment-timezone";
import VueTaggableSelect from "vue-taggable-select";

Vue.component("vue-taggable-select", VueTaggableSelect);
Vue.use(VueSweetalert2);
Vue.use(VueCookies);
// Vue.use(VueAxios, axios)
Vue.use(VueSession);
Vue.use(VeeValidate, {
  enableAutoClasses: true
});
Vue.use(Tooltip);
Vue.use(VueMoment, { moment });
Vue.use(Vueresource);
Vue.component("v-select", vSelect);
Vue.use(VueChatScroll);
Vue.use(Vuesax);
Vue.component(VueElementLoading);
Vue.use(VueWorker);
Vue.use(Loading);
Vue.config.productionTip = false;
Vue.http.interceptors.push((request, next) => {
  NProgress.start();
  NProgress.configure({});
  next(response => {
    NProgress.done();
  });
});
Vue.use(VueScrollTo);
Vue.use(VueIntro);
// Vue.http.headers.common['Access-Control-Allow-Origin'] = '*';
// Vue.http.headers.common['Access-Control-Request-Method'] = '*';
// Vue.http.headers.common['Access-Control-Request-Headers'] = '*';
Vue.directive("phone-number-format", function(el, binding) {
  var cleaned = ("" + el.value).replace(/\D/g, "");
  // var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    el.value = match[1] + "-" + match[2] + "-" + match[3];
  }
  return null;
  /* if(el.value.length==10){
		
		// var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
		var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
		if (match) {
			el.value =  match[1] + '-' + match[2] + '-' + match[3]
		}
		return null
	}else{
		if(el.value.length==3){
			var match = cleaned.match(/^(\d{3})$/)
			console.log(match)
			if (match) {
				el.value =  match[1] + '-' 
			}
		}
		if(el.value.length==7){
			var match = cleaned.match(/^(\d{3})(\d{3})$/)
			console.log(match)
			if (match) {
				el.value =  match[1] + '-' + match[2] + '-'
			}
		}
	} */
});

Vue.directive("numberOnly", {
  inserted(el) {
    el.oninput = event => {
      const formattedValue = event.target.value;
      el.value = formattedValue.replace(/[^0-9]*/g, "");
      if (formattedValue !== event.target.value.value) {
        event.stopPropagation();
      }
    };
  }
});

/* Vue.directive('phone-number-format', {
    inserted(el) {
      el.oninput = (event) => {
        const formattedValue = event.target.value;
        event.target.value = numeral(formattedValue).format('$ 0,0[.]00')
      };
    }
}); */

// VeeValidate.Validator.extend("email", {
//   getMessage: field => `The email is not valid. Please enter a valid email`,
//   validate: value => {
//     // const reg = ;
//     var strongRegex = new RegExp("^(\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$)", "g");
//     console.log(strongRegex.test(value), strongRegex);
//     return strongRegex.test(value);
//   }
// });

export const chatEngine = ChatEngineCore.create(
  {
    publishKey: process.env.VUE_APP_PUBNUB_PUBLISH_KEY,
    subscribeKey: process.env.VUE_APP_PUBNUB_SUBSCRIBE_KEY,
    secretKey: process.env.VUE_APP_PUBNUB_SECERET_KEY
    // logVerbosity: true,
  },
  {
    enableSync: true
    // debug:true
  }
);
Vue.use(PubNubVue, {
  subscribeKey: process.env.VUE_APP_PUBNUB_SUBSCRIBE_KEY,
  publishKey: process.env.VUE_APP_PUBNUB_PUBLISH_KEY
});
Vue.use(VueChatEngine, { chatEngine, store });
// axios.defaults.baseURL = process.env.VUE_BACKEND_URL
export const pusher = new Pusher(process.env.VUE_PUSHER_KEY, {
  cluster: process.env.VUE_PUSHER_CLUSTER
});
export const channel = pusher.subscribe("nexa-channel");
toastr.options = {
  closeButton: true,
  preventDuplicates: true,
  fadeOut: 3500
};

store.dispatch("getLocations");
store.dispatch("getProfessionalDesignation");
store.dispatch("getBrokerageList");
store.dispatch("getCompanyList");
/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  render: h => h(NexaMainPage)
  // created
}).$mount("#app");
