import Vue from 'vue'
import store from '@/store'

export default {
    getUserDetails(user_id,token) {
        return new Promise((resolve,reject)=>{
            Vue.http.get(process.env.VUE_BACKEND_URL+'user-details/'+user_id,{headers:{"Authorization":'Bearer '+token}})
                .then(response=>{
                    if(response.body.error==false){
                        var userDetails = store.state.currentUserDetails
                        response.body.data[0].token = userDetails.token
                        store.commit("currentUserDetailsAdd", response.body.data[0]);
                        sessionStorage.setItem(
                            "userAccessToken",
                            JSON.stringify(response.body.data[0])
                        );
                    }
                    resolve(response.body.data[0])
                })
                .catch(error=>{
                    resolve(error.body)
                })
        })
    },
    getProfileDetails(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'myprofile-edit',data,{headers:{"Authorization":'Bearer '+token}})
                .then(response=>{
                    if(response.body.error==false){
                        resolve(response.body)
                    }
                })
                .catch(error=>{
                    resolve(error.body)
                })
        })
    },
    getBrokers(search_value,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-exclude-search',{'key':search_value},{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error!=true){
                        resolve(res.body)
                    }
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    getUniqueStringDetails(string){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'free-for-ever-connect-details',{'unique_string':string})
                .then((res)=>{console.log(res.body.data)
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })

    },
    interestAddToList(data){
        Vue.http.get(process.env.VUE_BACKEND_URL+'interest-add-to-list?q='+data)
            .then((res)=>{
                return res.body

            })
            .catch(err=>{
                return err.body
            })
    },
    edgarMainListing(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'edgar-sub-category-listing',data)
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    edgarSubListing(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'edgar-interest-listing',data)
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    vSelectDatas(){
        return new Promise((resolve,reject)=>{
            Vue.http.get(process.env.VUE_BACKEND_URL+'location-dropdown')
                .then((res)=>{
                    if(res.body.error==false){
                        var options=[]
                        for(var i=0;i<res.body.data.length;i++){
                            options.push(res.body.data[i].location_name)
                        }
                        resolve(options)
                    }
                    
                })
                .catch(err=>{
                    console.log(err)
                })
        })
    },
    notificationFetching(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'notification-fetching',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    agentLeadDataListing(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'lead-data-listing',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getStartedBuildTeam(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'get-started-build-team',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body.data)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    agentLeadDataGet(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'lead-data-edit',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },agentLeadDataGetForGetStarted(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'lead-data-edit-get-started',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    agentLeadDataDetailsGet(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'lead-data-details',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    brokerSearch(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    brokerDetails(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-listing',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getCreditDetails(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'credit-details',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    singleBrokerView(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-view',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getCreditLog(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'credit-log',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getPaymentDetails(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'current-subscription',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    favouriteListing(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'favourite-listing',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    consolidatedEmailStatus(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'email-status',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    consolidatedSmsStatus(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'sms-status',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getBillingInfo(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'billing-info-edit',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getPaymentLog(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'payment-log',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    excludedBrokersList(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'excluded-brokers-list',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getMatchingLeadDataConnected(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-lead-data-matching-list',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getUnderWriterMatchingLeadDataConnected(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'underwriter-lead-data-matching-list',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getMarketingRepsMatchingLeadDataConnected(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'marketing-reps-lead-data-matching-list',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getMatchingLeadDataPending(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-lead-data-matching-list-pending',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }else{
                        reject(res.body.message)
                    }
                })
                .catch(err=>{
                    reject(err)
                })
        })
    },
    getUnderWriterMatchingLeadDataPending(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'underwriter-lead-data-matching-list-pending',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getMarketingRepsMatchingLeadDataPending(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'marketing-reps-lead-data-matching-list-pending',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    leadDataRequestList(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'lead-data-requests-lists',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    connectedStatus(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'connect-status',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    // console.log(res.body.data[0].connect_status,res.body.data.connect_status)
                    if(res.body.error==false){
                        // store.commit('ConnectedStatus',res.body.data)
                        resolve(res.body)
                    }else if(res.body.error==true){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    headerDetails(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-connect',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    userEmailAvalibility(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'user-availability',data)
                .then((res)=>{
                    resolve(res.body.error)
                })
                .catch((err)=>{
                    resolve(res.body.error)
                })
        })
    },
    brokerMatchingDataSearchPending(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-match-list-pending-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    brokerMatchingDataSearchConnected(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-match-list-connected-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    getMatchingLeadDataPendingFilter(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'matching-leaddata-filer-pending',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getMatchingLeadDataConnectedFilter(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'matching-leaddata-filer-connected',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    UnderwriterMatchingDataSearchPending(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'underwriter-match-list-pending-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    UnderwriterMatchingDataSearchConnected(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'underwriter-match-list-connected-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    getUnderwriterMatchingLeadDataPendingFilter(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'underwriter-matching-leaddata-filer-pending',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getUnderwriterMatchingLeadDataConnectedFilter(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'underwriter-matching-leaddata-filer-connected',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    
    MarketingRepsMatchingDataSearchPending(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'marketing-reps-match-list-pending-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    MarketingRepsMatchingDataSearchConnected(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'marketing-reps-match-list-connected-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    getMarketingRepsMatchingLeadDataPendingFilter(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'marketing-reps-matching-leaddata-filer-pending',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getMarketingRepsMatchingLeadDataConnectedFilter(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'marketing-reps-matching-leaddata-filer-connected',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },

    getBrokerLeadDataFilterLists(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-lead-data-filter-listing',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },

    globalTeamListing(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'global-team-listing',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getSubTeamDetails(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'sub-team-listing',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getSubTeamMembersDetails(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'sub-team-members-listing',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getSubTeamMembersDetailsSearch(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'sub-team-members-listing-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getGlobalTeamMemberDetailForSubteam(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'global-team-member-details-for-sub-team',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getGlobalTeamMemberDetailForSubteamFilter(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'global-team-member-details-for-sub-team-filter',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getGlobalTeamMemberDetailForSubteamSearch(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'global-team-member-details-for-sub-team-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getGlobalMembersSearch(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'global-team-listing-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    getSubTeamMembersSearch(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'members-not-in-team-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    membersNotInTeam(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'members-not-in-team',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    TeamsAmAdded(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'teams-am-added',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body)
                    }
                })
                .catch(err=>{
                    resolve(err.body)
                })
        })
    },
    QuickAccountCreation(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'quick-account-creation-details',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    subteamListing(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'sub-team-listing-lead-data',data)
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    userSearchForLeadData(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'users-search',data)
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    getGlobalTeamDetails(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'get-gobal-team-details',data)
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    createTeamListing(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'team-creation-listing',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    createTeamListingSearch(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'team-creation-listing-search',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    teamMemberCount(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'team-member-count',data)
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body.data)
                    }else{
                        reject(0)
                    }
                })
                .catch((err)=>{
                    reject(err.body)
                })
        })
    },
    LinkedinLoginTest(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'test',data)
                .then((res)=>{
                    resolve(res.body.data)
                })
                .catch((err)=>{
                    reject(err.body)
                })
        })
    },
    LinkedinLogin(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'linkedin-login',data)
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body.data)
                    }else{
                        reject(res.body.message)
                    }
                    
                })
                .catch((err)=>{
                    reject(err)
                })
        })
    },
    LinkedinSignup(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'linkedin-signup',data)
                .then((res)=>{
                    if(res.body.error==false){
                        resolve(res.body.data)
                    }else{
                        reject(res.body.message)
                    }
                })
                .catch((err)=>{
                    reject(err)
                })
        })
    },
    UserTypeCheck(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'user-type-check',data)
                .then((res)=>{
                    resolve(res)
                })
                .catch((err)=>{
                    reject(err)
                })
        })
    },
    UserTypeCheckStatus(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'user-type-check',data)
                .then((res)=>{
                    resolve(res.body.error)
                })
                .catch((err)=>{
                    reject(err)
                })
        })
    },
    GlobalTeamSearch(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'global-team-search',data)
                .then((res)=>{
                    resolve(res)
                })
                .catch((err)=>{
                    reject(err)
                })
        })
    },
}