import Vue from 'vue'

export default{
    postProfile(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'get-started-myprofile', data, {headers:{"Authorization":'Bearer '+token}})
            .then(function (response) {
                if(response.body.error==false){
                    resolve(response.body)
                }else{
                    resolve(response.body)
                }
            })
            .catch(function (error) {
                resolve(error.body)
            });
        })
    },
    imageDataSubmit(image) {
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        let formData = new FormData();
        formData.append('image', image);
        
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'image', formData, config)
            .then(function (response) {
                resolve(response.body.data)
            })
            .catch(function (error) {
                console.log(error)
            });
        })
    },
    fileUploadSubmit(image) {
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        let formData = new FormData();
        formData.append('market_list', image);
        
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'file-attachment', formData, config)
            .then(function (response) {
                resolve(response.body.data)
            })
            .catch(function (error) {
                console.log(error)
            });
        })
    },
    chatFileUploadSubmit(image) {
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        let formData = new FormData();
        formData.append('chat_file', image);
        
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'chat-file-attachment', formData, config)
            .then(function (response) {
                resolve(response.body.data)
            })
            .catch(function (error) {
                reject(error)
            });
        })
    },
    LeadDataSubmit(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'leaddata-register',data,{headers:{"Authorization":'Bearer '+token}})
					.then((res)=>{
						if(res.body.error==false){
                            resolve(res.body)
                        }
					})
					.catch((err)=>{
						resolve(err.body)
					})
        })
    },
    inviteSignup(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'free-for-ever-signup', data)
                .then(response => {
                    resolve(response.body)
                })
                .catch(err =>{
                    resolve(err.body)
                })
        })
    },
    signup(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'user-signup', data)
                .then(response => {
                    resolve(response.body)
                })
                .catch(err =>{
                    resolve(err.body)
                })
        })
    },
    brokerLeadDataSubmit(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-lead-data-register',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    notificationMarkRead(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'notification-marking-read',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    favouriteRequestSending(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'favourite-request-sending',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    cancelFavouriteRequest(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'cancel-favourite-request',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    updateProfile(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'myprofile-update',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    updatePackage(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'upgrade-subscription',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    favouriteRemove(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'favourite-broker-remove',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    consolidatedEmailUpdate(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'email-settings',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    consolidatedSmsUpdate(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'sms-settings',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    billingInfoAdd(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'billing-info',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    billingInfoUpdate(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'billing-info-update',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    leadDataUpdate(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'lead-data-updating',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    acceptChatRequest(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'agent-accept-broker-chat-request',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    cancelChatRequest(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'cancel-broker-chat-request',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    sendChatRequest(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-chat-request',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    linkedinConnect(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'linkedin-connect',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    forgotPasswordEmailSend(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'forgot-password',data)
                .then((res)=>{
                    if(res.body.error == false){
                        resolve(res.body)
                    }else{
                        reject(res.body)
                    }
                })
                .catch((err)=>{
                    reject(err.body)
                })
        })
    },
    resetPassword(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'reset-password',data)
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    changePassword(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'change-password',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    UnderwriterLeaddataRegister(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'underwriter-lead-data-register',data)
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    marketingRepsLeaddataRegister(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'marketing-reps-lead-data-register',data)
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    FreeForEverInvitations(data){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'free-for-ever-invitation',data)
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    accountDelete(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'account-delete',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    brokerLeadDataDelete(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'delete-broker-lead-data',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    brokerLeadDataPreferenceEdit(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'broker-lead-data-preference-edit',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    deleteGlobalMembers(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'delete-global-members',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    createSubTeam(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'create-sub-team',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    deleteSubTeam(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'delete-sub-team',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    deleteSubTeamMember(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'delete-sub-team-member',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    AddSubTeamMember(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'add-sub-team-members',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    AddGlobalMember(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'add-global-team-members',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    DirectInvitationToSubTeam(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'direct-invite-sub-team',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    SubTeamNameUpdate(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'sub-team-name-update',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    AddMembersToCreatedSubTeam(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'add-members-to-created-sub-team',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    QuickAccountCreationUpdate(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'quick-account-creation-details-udate',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    TeamMembersAdding(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'team-members-adding',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    LogOut(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'user-logout',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
    UpdateCredentials(data,token){
        return new Promise((resolve,reject)=>{
            Vue.http.post(process.env.VUE_BACKEND_URL+'update-credentials',data,{headers:{"Authorization":'Bearer '+token}})
                .then((res)=>{
                    resolve(res.body)
                })
                .catch((err)=>{
                    resolve(err.body)
                })
        })
    },
}