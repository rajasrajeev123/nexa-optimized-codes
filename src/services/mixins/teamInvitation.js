function isBelowThreshold(currentValue) {
  return currentValue == false;
}
import store from "@/store";
import postMethods from "@/services/api/postMethods";
export const teamInvitation = {
  data() {
    return {
      invitationNameCreate: [],
      invitationemailcreateerror: [],
      invitationemailcreatemessage: [],
      invitationnamecreateerror: [],
      invitationnamecreatemessage: [],
      invitationEmailCreate: [],
      invitationCheckBox: [],
      arrayValueCreate: 0,
      buildButtonValue: "Send Invites Now",
      buildButton: false,
      buildButtonValue2: "Go to Messaging Center!",
      buildButton2: false
    };
  },
  watch: {
    invitationNameCreate(newval, oldval) {
      for (var i = 0; i < newval.length; i++) {
        if (newval[i]) {
          this.invitationnamecreateerror[i] = false;
          this.invitationnamecreatemessage[i] = "";
        } else {
          this.invitationnamecreateerror[i] = true;
          this.invitationnamecreatemessage[i] = "Required";
        }
      }
    },
    invitationEmailCreate(newval, oldval) {
      for (var i = 0; i < newval.length; i++) {
        if (newval[i] && this.validEmail(newval[i])) {
          this.invitationemailcreateerror[i] = false;
          this.invitationemailcreatemessage[i] = "";
        } else {
          if (!this.validEmail(newval[i])) {
            this.invitationemailcreateerror[i] = true;
            this.invitationemailcreatemessage[i] = "Needs a valid email";
          } else {
            this.invitationemailcreateerror[i] = false;
            this.invitationemailcreatemessage[i] = "";
          }
        }
      }
    }
  },
  computed: {
    userDetails() {
      return store.state.currentUserDetails;
    }
  },
  methods: {
    reset() {
      this.invitationNameCreate = [];
      this.invitationemailcreateerror = [];
      this.invitationemailcreatemessage = [];
      this.invitationnamecreateerror = [];
      this.invitationnamecreatemessage = [];
      this.invitationEmailCreate = [];
      this.invitationCheckBox = [];
      this.arrayValueCreate = 0;
      this.buildButtonValue = "Send Invites Now";
      this.buildButton = false;
      this.buildButtonValue2 = "Go to Messaging Center!";
      this.buildButton2 = false;
    },
    validEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    addCheckboxCreate() {
      this.arrayValueCreate = this.arrayValueCreate + 1;
    },
    deleteCheckboxCreate(i) {
      this.arrayValueCreate = this.arrayValueCreate - 1;
      this.invitationEmailCreate.pop(i);
      this.invitationNameCreate.pop(i);
    },
    inviteCreateTeamValidationCheck() {
      if (
        this.invitationemailcreateerror.every(isBelowThreshold) &&
        this.invitationemailcreatemessage.every(isBelowThreshold)
      ) {
        var data = {
          name: this.invitationNameCreate,
          email: this.invitationEmailCreate,
          user_id: this.userDetails.user_id,
          user_type: this.userDetails.user_type,
          progress_status: 4,
          flag: 1
        };
        this.buildButtonValue = "Loading...";
        this.buildButton = true;
        postMethods
          .postProfile(data, this.userDetails.token)
          .then(res => {
            if (res.error == false) {
              if (res.data.length > 0) {
                var message = "";
                if (res.data.length == 1) {
                  message = `${
                    res.data[0]
                  } is already in nexa and we have added them to your global team.`;
                } else {
                  message = `${res.data.toString()} are already in nexa and we have added them to your global team.`;
                }
                toastr.success(message);
              } else {
                toastr.success("Invitation sent successfully");
              }

              this.buildButtonValue = "Send Invites Now";
              this.buildButton = false;
              this.reset();
              $("#invitationModal").modal("hide");
            } else {
              this.buildButtonValue = "Send Invites Now";
              this.buildButton = false;
              toastr.error(res.message);
            }
          })
          .catch(err => {
            $("#invitationModal").modal("hide");
            toastr.error(err.message);
          });
      } else {
        this.buildButtonValue = "Send Invites Now";
        this.buildButton = false;
        toastr.error("Some mandatory fields are missing");
      }
    }
  }
};
