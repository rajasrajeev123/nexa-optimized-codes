import store from '@/store';
export const brokerQuestionnaireMixin = {
    data(){
        return{
            InterestListDataUrl:process.env.VUE_BACKEND_URL+'interest-autocomplete',
            locationList: store.state.states,
            marketSearch: null,
            agentIntrest: null,
            operateMultipleState: [],
            opererror: false,
            opermessage: '',
            exclude_state: null,
            accountLocation: [],
            coastalExposure: null,
            line_coverage: [],
            othererror: false,
            othermessage: '',
            otherData: null,
            annualCost: null,
            value_sale: numeral(1000000).format('$ 0,0[.]00'),
            annualerror: false,
            annualmessage: '',
            annualPayroll: null,
            value_payroll: numeral(1000000).format('$ 0,0[.]00'),
            annualperror: false,
            annualpmessage: null,
            rushRequests: null,
            newVenture: null,
            disabledButton: false,
            primaryInterest:0,
            agentIntrest: null,
            marketSearch: null,
            email: '',
            eerror: false,
            emessage: '',
            emailerror: false,
            emailmessage: '',
            emailAleady: false,
            firstname: '',
            lastname: '',
            fnameerror: false,
            fnamemessage: '',
            lnameerror: false,
            lnamemessage: '',
            password: null,
            passworderror: false,
            passwordmessage: '',
        }
    },
    watch: {
        firstname(newval,oldval){
            if(newval==null||newval==""){
                this.fnameerror=true
                // this.addValidation(20, 'delete')
                this.fnamemessage='First name Required !'
                this.disabledButton=true
            }else {
                if(newval.length>=2){
                    this.fnameerror=false
                    // this.addValidation(20, 'add')
                    this.fnamemessage=''
                    this.disabledButton=false
                }else{
                    this.fnameerror=true
                    // this.addValidation(20, 'delete')
                    this.fnamemessage='Length should be minimum of 2 characters !'
                    this.disabledButton=true
                }
            }
        },
         
        email(newval,oldval){
            if(newval==null||newval==""){
                this.emailerror=true
                this.email=null
                this.emailAleady = false
                // this.addValidation(20, 'delete')
                this.emailmessage='Email Required !'
                this.disabledButton=true
            }else {
                if((newval!=null||newval!="")&&(this.validEmail(newval))){
                    this.emailValidationCheck()
                    this.emailAleady = false
                    this.emailerror=false
                    // this.addValidation(20, 'add')
                    this.emailmessage=''
                    this.disabledButton=false
                }else{
                    this.emailerror=true
                    // this.addValidation(20, 'delete')
                    this.emailmessage='Invalid Email please try other one!'
                    this.emailAleady = false
                    this.disabledButton=true
                }
            }
        },
 
        password(newval,oldval){
            var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{5,})");
            // console.log(strongRegex.test(newval))
            if(newval==null||newval==""){
                this.passwordmessage='Required !'
                this.passworderror=true
                this.disabledButton=true
            }else{
                if(strongRegex.test(newval)==true){
                    this.passwordmessage=''
                    this.passworderror=false
                    this.disabledButton=false
                }else{
                    this.passwordmessage='The password must contain at least: minimum 5 characters, maximum 10 characters, 1 uppercase letter, 1 lowercase letter and one special character'
                    this.passworderror=true
                    this.disabledButton=true
                }
            }
        },
        operateMultipleState(newval,oldval){
            if(newval==[]){
                this.opererror=true
                this.opermessage='Required !'
                this.disabledButton=true
            }else{
                this.opererror=false
                this.opermessage=''
                this.disabledButton=false
            }
        },
        exclude_state(newval,oldval){
            if(newval==1 || newval=="1"){
                this.disabledButton=true
                if(this.accountLocation.length>0){
                    this.disabledButton=false
                }
            }else{
                this.accountLocation = []
                this.disabledButton=false
            }
        },
        accountLocation(newval,oldval){
            if(newval!=null||newval!=""){
                this.disabledButton=false
            }else{
                this.disabledButton=true
            }
        },
        coastalExposure(newval,oldval){
            if(newval=='1'||newval=='2'||newval=='3'||newval=='4'){
                this.disabledButton=false
            }else{
                this.disabledButton=true
            }
        },
        line_coverage(newval,oldval){
            var error = false
            if(newval.indexOf(7)>-1 && (this.otherData==null||this.otherData=="")){
                error = true
                this.othererror=true
                this.othermessage='Required !'
                this.disabledButton=true
            }else{
                this.othererror=false
                this.othermessage=''
                this.disabledButton=false
            }
            if(newval.length>0 && error ==false){
                this.disabledButton=false
            }else{
                this.otherData=null
                this.disabledButton=true
            }	

        },
        otherData(newval,oldval){
            if(this.line_coverage.indexOf(7)>-1){
                if(newval==null||newval==""){
                    this.othererror=true
                    this.othermessage='Required !'
                    this.disabledButton=true
                }else{
                    if(!isNaN(newval)){
                        this.othererror=true
                        this.othermessage='Number Not Required !'
                        this.disabledButton=true
                    }else{
                        this.othererror=false
                        this.othermessage=''
                        this.disabledButton=false
                    }
                }
            }else{
                this.otherData=null
            }
        },
        annualCost(newval,oldval){
            if(newval=='0'){
                this.disabledButton=false
                this.annualCostValue=null
            }else if(newval=='1'){
                this.disabledButton=false
            }else{
                this.disabledButton=true
                
            }
        },
        annualPayroll(newval,oldval){
            if(newval=='0'){
                this.disabledButton=false
                this.annualPayrollValue=null
            }else if(newval=='1'){
                this.disabledButton=false
            }else{
                this.disabledButton=true
                
            }
        },

        value_sale(newval,oldval){
            
            if(newval==null||newval==""){
                this.annualerror=true
                this.annualmessage='Required !'
                this.disabledButton=true
            }else{
                newval = newval.replace(/\D/g,'');
                if(isNaN(newval)){
                    this.value_sale=null
                    this.annualerror=true
                    this.annualmessage='Number Required !'
                    this.disabledButton=true
                }else if((newval%1)!=0){
                    
                    this.annualerror=true
                    this.annualmessage='Digit values not required !'
                    this.disabledButton=true
                }else{
                    var datas = numeral(newval).format('$ 0,0[.]00')
                    this.value_sale = datas
                    this.annualerror=false
                    this.annualmessage=''
                    this.disabledButton=false
                }
            }
        },
        value_payroll(newval,oldval){
            
            if(newval==null||newval==""){
                this.annualperror=true
                this.annualpmessage='Required !'
                this.disabledButton=true
            }else{
                newval = newval.replace(/\D/g,'');
                if(isNaN(newval)){
                    this.value_payroll=null
                    this.annualperror=true
                    this.annualpmessage='Number Required !'
                    this.disabledButton=true
                }else if((newval%1)!=0){
                    this.annualperror=true
                    this.annualpmessage='Digit value not required !'
                    this.disabledButton=true
                }else{
                    var datas = numeral(newval).format('$ 0,0[.]00')
                    this.value_payroll = datas
                    this.annualperror=false
                    this.annualpmessage=''
                    this.disabledButton=false
                }
            }
        },

        rushRequests(newval,oldval){
            if(newval!=null||newval!=""){
                this.disabledButton=false
            }else{
                this.disabledButton=true
            }
        },
        newVenture(newval,oldval){
            if(newval!=null||newval!=""){
                this.disabledButton=false
            }else{
                this.disabledButton=true
            }
        },
    }
}