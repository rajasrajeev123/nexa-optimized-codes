import ThumbsUp from '@/components/NexaQuestionnaire/ThumbsUp'
import store from '@/store';
import postMethods from "@/services/api/postMethods.js";
import getMethods from "@/services/api/getMethods.js";
import { bus } from '@/main';
import VeeValidate from "vee-validate";
VeeValidate.Validator.extend("max_value_custom", {
  getMessage: field => `The value must be less than current year.`,
  validate: value => {
    var d = new Date();
    var n = d.getFullYear();
    if (value > n) {
      return false;
    } else {
      return true;
    }
  }
});
export const profileInformation =  {
    components:{
      ThumbsUp
    },
    data(){
        return{
            locationList: store.state.states,
            professsionalDesignation: store.state.professionalDesignations,
            imageUploaded: false,
            image: null,
            firstname: '',
            lastname: '',
            agentName: null,
            otherAgent: '',
            street1: '',
            street2: '',
            suite: '',
            agencyName: '',
            city: '',
            addressState: '',
            zip: '',
            agencyPhoneNumber: '',
            phoneNumber: '',
            stateLicenced: [],
            yearLicenced: '',
            agentLicence: '',
            otherProfessionalDesignation: '',
            professionDesign: [],
            brokerageName: [],
            companyName: [],
            website: '',
            brokerageSpeciality: '',
            about: '',
            brokerPhoneNumber: '',
            thumbsUp: false,
        }
    },
    mounted(){
      bus.$on('broker-login-success',()=>{
            if(this.userDetails.user_type==2){
                this.thumbsUp=false
            }
        })
      bus.$on('thumbsUpDeactivate',()=>{
            this.thumbsUp=false
        })
        if (this.userDetails.progress_status == 1) {
            this.firstname = this.userDetails.first_name;
            this.lastname = this.userDetails.last_name;
            if (this.userDetails.profile_pic != null) {
                this.image = process.env.VUE_IMAGE_URL + this.userDetails.profile_pic;
            } else {
                this.image = this.userDetails.linkedin_pic;
            }
            bus.$emit('sideBarUp',0);
        } else if (this.userDetails.progress_status == 4 || this.userDetails.progress_status == 2) {
            this.firstname = this.userDetails.first_name;
            this.lastname = this.userDetails.last_name;
            this.getProfileDetails();
            bus.$emit('sideBarUp',1);
            if (this.userDetails.profile_pic != null) {
                this.image = process.env.VUE_IMAGE_URL + this.userDetails.profile_pic;
            } else {
                this.image = this.userDetails.linkedin_pic;
            }
        } else if (this.userDetails.progress_status == 6 || this.userDetails.progress_status == 5) {
            this.firstname = this.userDetails.first_name;
            this.lastname = this.userDetails.last_name;
            this.getProfileDetails();
            this.thumbsUp=true
            if (this.userDetails.profile_pic != null) {
                this.image = process.env.VUE_IMAGE_URL + this.userDetails.profile_pic;
            } else {
                this.image = this.userDetails.linkedin_pic;
            }
        }
        $(this.$refs.agentNameSelectPicker).selectpicker({ refresh: true, dropupAuto: false });
        $(this.$refs.stateLicencedSelectPicker).selectpicker({ refresh: true, dropupAuto: false });
        $(this.$refs.addressStateSelectPicker).selectpicker({ refresh: true, dropupAuto: false });
        $(this.$refs.professionalDesignationSelectPicker).selectpicker({ refresh: true, dropupAuto: false });
        $(this.$refs.brokerageNameSelectPicker).selectpicker({ refresh: true, dropupAuto: false });
    },
    updated(){
        $(this.$refs.agentNameSelectPicker).selectpicker({ refresh: true, dropupAuto: false });
        $(this.$refs.stateLicencedSelectPicker).selectpicker({ refresh: true, dropupAuto: false });
        $(this.$refs.addressStateSelectPicker).selectpicker({ refresh: true, dropupAuto: false });
        $(this.$refs.professionalDesignationSelectPicker).selectpicker({ refresh: true, dropupAuto: false });
        $(this.$refs.brokerageNameSelectPicker).selectpicker({ refresh: true, dropupAuto: false });
    },
    computed:{
        userDetails() {
            return store.state.currentUserDetails;
        },
        excludeBrokerageList() {
            return store.state.brokerageList;
        },
        excludeCompanyList() {
            return store.state.companyList;
        }
    },
    methods:{
        getProfileDetails() {
            var data = {
                user_type: this.userDetails.user_type,
                agent_id: this.userDetails.user_id,
                broker_id: this.userDetails.user_id,
                user_type: this.userDetails.user_type
            };
            getMethods.getProfileDetails(data, this.userDetails.token).then(res => {
                if (res.error == false) {
                var datas = [];
                var datas_pro = [];
                if (this.userDetails.user_type == 1) {
                    this.firstname = res.data[0][0].first_name != null ? res.data[0][0].first_name : "";
                    this.lastname = res.data[0][0].last_name != null ? res.data[0][0].last_name : "";
                    this.agentName = res.data[0][0].agent_type != null ? res.data[0][0].agent_type : "";
                    if (this.agentName == 5) {
                    this.otherAgent = res.data[0][0].agent_type_others != null ? res.data[0][0].agent_type_others : "";
                    }
                    this.agencyName = res.data[0][0].agency_name != null ? res.data[0][0].agency_name : "";
                    this.street1 = res.data[0][0].street1 != null ? res.data[0][0].street1 : "";
                    this.street2 = res.data[0][0].street2 != null ? res.data[0][0].street2 : "";
                    this.suite = res.data[0][0].suite != null ? res.data[0][0].suite : "";
                    this.city = res.data[0][0].city != null ? res.data[0][0].city : "";
                    this.addressState = res.data[0][0].state != null ? res.data[0][0].state : "";
                    this.zip = res.data[0][0].zipcode != null ? res.data[0][0].zipcode : "";
                    this.phoneNumber = res.data[0][0].phone_number != null ? res.data[0][0].phone_number : "";
                    this.agencyPhoneNumber = res.data[0][0].agency_phone_number != null ? res.data[0][0].agency_phone_number : "";
                    this.agentLicence = res.data[0][0].license != null ? res.data[0][0].license : "";
                    this.yearLicenced = res.data[0][0].year_of_license != null ? res.data[0][0].year_of_license : "";
                    var data = res.data[1];
                    if (data != null) {
                    var arr = data.split(",");
                    arr.forEach(element => {
                        datas.push(element);
                    });
                    this.stateLicenced = datas;
                    }
                    var data_pro = res.data[2];
                    if (res.data[2] != "") {
                    var arr = data_pro.split(",");
                    arr.forEach(element => {
                        datas_pro.push(element);
                    });
                    this.professionDesign = datas_pro;
                    }
                    if (res.data[3] != "") {
                    this.otherProfessionalDesignation = res.data[3][0].comment;
                    }
                    if (res.data[0][0].profile_pic != null) {
                    this.image = process.env.VUE_IMAGE_URL + res.data[0][0].profile_pic;
                    } else {
                    this.image = res.data[0][0].linkedin_pic;
                    }
                } else {
                    this.firstname = res.data[0].first_name != null ? res.data[0].first_name : "";
                    this.lastname = res.data[0].last_name != null ? res.data[0].last_name : "";
                    this.brokerageName = res.data[0].brokerage_name != null ? res.data[0].brokerage_name : "";
                    this.street1 = res.data[0][0].street1 != null ? res.data[0][0].street1 : "";
                    this.street2 = res.data[0][0].street2 != null ? res.data[0][0].street2 : "";
                    this.addressState = res.data[0].state != null ? res.data[0].state : "";
                    this.suite = res.data[0].suite != null ? res.data[0].suite : "";
                    this.city = res.data[0].city != null ? res.data[0].city : "";
                    this.zip = res.data[0].zipcode != null ? res.data[0].zipcode : "";
                    this.brokerageSpeciality = res.data[0].speciality != null ? res.data[0].speciality : "";
                    this.about = res.data[0].about != null ? res.data[0].about : "";
                    this.brokerPhoneNumber = res.data[0].phone_number != null ? res.data[0].phone_number : "";
                    this.website = res.data[0].website != null ? res.data[0].website : "";
                    if (res.data[0].profile_pic != null) {
                    this.image = process.env.VUE_IMAGE_URL + res.data[0].profile_pic;
                    } else {
                    this.image = res.data[0].linkedin_pic;
                    }
                }
                } else {
                toastr.error(res.message);
                }
            });
        },
        getStartedProfileValidationAgent() {
        this.$validator.validateAll("get_started_profile_validation_agent").then(result => {
            if (result) {
            var data = {
                user_id: this.userDetails.user_id,
                user_type: this.userDetails.user_type,
                first_name: this.firstname,
                last_name: this.lastname,
                agent_type: { type: this.agentName, agent_type_others: this.otherAgent },
                agency_name: this.agencyName,
                linkedin_pic: this.userDetails.linkedin_pic,
                image: this.userDetails.profile_pic,
                street_1: this.street1,
                street_2: this.street2,
                suite: this.suite,
                address_state: this.addressState,
                city: this.city,
                zipcode: this.zip,
                phone_number: this.phoneNumber,
                license: this.agentLicence,
                fax: this.fax,
                state: this.stateLicenced,
                progress_status: 2,
                agency_phone_number: this.agencyPhoneNumber,
                year_of_license: this.yearLicenced,
                professional_designations: { designation_id: this.professionDesign, comment: this.otherProfessionalDesignation }
            };
            if (this.imageUploaded == true) {
                postMethods.imageDataSubmit(this.image2).then(res => {
                data.image = res;
                this.userDetails.profile_pic = res;
                this.userDetails.first_name = this.firstname;
                this.userDetails.last_name = this.lastname;
                store.commit("currentUserDetailsAdd", this.userDetails);
                postMethods.postProfile(data, this.userDetails.token).then(res => {
                    if (res.error == false) {
                    toastr.success(res.message);
                    this.thumbsUp=true
                    } else {
                    toastr.error(res.message);
                    }
                });
                });
            } else {
                postMethods.postProfile(data, this.userDetails.token).then(res => {
                if (res.error == false) {
                    toastr.success(res.message);
                    this.thumbsUp=true
                } else {
                    toastr.error(res.message);
                }
                });
            }
            } else {
            toastr.error("Missing Fields. Please see below and try again.");
            }
        });
        },
        getStartedProfileValidationBroker() {
        this.$validator.validateAll("get_started_profile_validation_broker").then(result => {
            if (result) {
            var data = {
                user_id: this.userDetails.user_id,
                first_name: this.firstname,
                last_name: this.lastname,
                progress_status: 2,
                brokerage_name: this.brokerageName,
                company_name: this.companyName,
                about: this.about,
                linkedin_pic: this.userDetails.linkedin_pic,
                image: this.userDetails.profile_pic,
                user_type: this.userDetails.user_type,
                street_1: this.street1,
                street_2: this.street2,
                suite: this.suite,
                address_state: this.addressState,
                city: this.city,
                zipcode: this.zip,
                website: this.website,
                phone_number: this.brokerPhoneNumber,
                speciality: this.brokerageSpeciality
            };
            if (this.imageUploaded == true) {
                postMethods.imageDataSubmit(this.image2).then(res => {
                data.image = res;
                this.userDetails.profile_pic = res;
                this.userDetails.first_name = this.firstname;
                this.userDetails.last_name = this.lastname;
                store.commit("currentUserDetailsAdd", this.userDetails);
                postMethods.postProfile(data, this.userDetails.token).then(res => {
                    if (res.error == false) {
                    toastr.success(res.message);
                    store.dispatch("getBrokerageList");
                    store.dispatch("getCompanyList");
                    bus.$emit('sideBarUp',1)
                    } else {
                    toastr.error(res.message);
                    }
                });
                });
            } else {
                postMethods.postProfile(data, this.userDetails.token).then(res => {
                if (res.error == false) {
                    toastr.success(res.message);
                    store.dispatch("getBrokerageList");
                    store.dispatch("getCompanyList");
                    bus.$emit('sideBarUp',1)
                } else {
                    toastr.error(res.message);
                }
                });
            }
            } else {
            toastr.error("Missing Fields. Please see below and try again.");
            }
        });
        },
        onImageChange($event) {
        if ($event.target.files[0]) {
            this.image = $event.target.files[0];
            this.image2 = $event.target.files[0];
            if (this.image) {
            this.imageUploaded = true;
            } else {
            this.imageUploaded = false;
            }

            this.createImage(this.image);
        }
        },
        createImage(file) {
        var image = new Image();
        var reader = new FileReader();

        reader.onload = e => {
            this.image = e.target.result;
        };
        reader.readAsDataURL(file);
        },
    },
}