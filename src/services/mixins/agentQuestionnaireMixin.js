import store from '@/store';
export const agentQuestionnaireMixin = {
    data(){
        return{
            agentData: JSON.parse(localStorage.getItem('agentData')),
            accountLocation: '',
            locationList: store.state.states,
            multipleOperates: null,
            operateMultipleState: [],
            disabledButton: false,
            opererror: false,
            opermessage: '',
            coastalExposure: null,
            line_coverage: [],
            otherData: null,
            othererror: false,
            othermessage: '',
            umbrellaLimit: null,
            umbrerror: false,
            umbrmessage: '',
            propertyEstimate: null,
            properror: false,
            propmessage: '',
            annualCost: '',
            annualerror: false,
            annualmessage: '',
            annualPayroll: '',
            annualpmessage: '',
            annualperror: false,
            recordAccount: null,
            renewalData: null,
            newVenture: null,
            accordApplications: null,
            supplementalApplications: null,
            lossRuns: null,
            otherBrokerSubmitted: null,
            marketList: null,
            file:null,
            file2:null,
            keyInformations: '',
            keyinfoerror: false,
            keyinfomessage: '',
            rushRequests: null,
            coverageDate: null,
            coverageerror: false,
            coveragemessage: '',
            targetPremium: null,
            premiumTarget: null,
            premmessage: '',
            premerror: false,
            agentIntrest: null,
            marketSearch: null,
            email: '',
            eerror: false,
            emessage: '',
            emailerror: false,
            emailmessage: '',
            emailAleady: false,
            firstname: '',
            lastname: '',
            fnamerror: false,
            fnamemessage: '',
            lnamerror: false,
            lnamemessage: '',
            password: null,
            passworderror: false,
            passwordmessage: '',
            searchInput: [],
        }
    },
    watch: {
        searchInput(newval,oldval){
            
        },
        firstname(newval,oldval){
            if(newval==null||newval==""){
                this.fnameerror=true
                // this.addValidation(20, 'delete')
                this.fnamemessage='First name Required !'
                this.disabledButton=true
            }else {
                if(newval.length>=2){
                    this.fnameerror=false
                    // this.addValidation(20, 'add')
                    this.fnamemessage=''
                    this.disabledButton=false
                }else{
                    this.fnameerror=true
                    // this.addValidation(20, 'delete')
                    this.fnamemessage='Length should be minimum of 2 characters !'
                    this.disabledButton=true
                }
            }
        },
         
        email(newval,oldval){
            if(newval==null||newval==""){
                this.emailerror=true
                this.email=null
                this.emailAleady = false
                // this.addValidation(20, 'delete')
                this.emailmessage='Email Required !'
                this.disabledButton=true
            }else {
                if((newval!=null||newval!="")&&(this.validEmail(newval))){
                    this.emailValidationCheck()
                    this.emailAleady = false
                    this.emailerror=false
                    // this.addValidation(20, 'add')
                    this.emailmessage=''
                    this.disabledButton=false
                }else{
                    this.emailerror=true
                    // this.addValidation(20, 'delete')
                    this.emailmessage='Invalid Email please try other one!'
                    this.emailAleady = false
                    this.disabledButton=true
                }
            }
        },
 
        password(newval,oldval){
            var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{5,})");
            // console.log(strongRegex.test(newval))
            if(newval==null||newval==""){
                this.passwordmessage='Required !'
                this.passworderror=true
                this.disabledButton=true
            }else{
                if(strongRegex.test(newval)==true){
                    this.passwordmessage=''
                    this.passworderror=false
                    this.disabledButton=false
                }else{
                    this.passwordmessage='The password must contain at least: minimum 5 characters, maximum 10 characters, 1 uppercase letter, 1 lowercase letter and one special character'
                    this.passworderror=true
                    this.disabledButton=true
                }
            }
        },
        multipleOperates(newval,oldval){
            if(newval=='1'){
                if(this.operateMultipleState.length==0){
                    this.opererror=true
                    this.opermessage='Required !'
                    this.disabledButton=true
                }else{
                    this.operperror=false
                    this.opermessage=''
                    this.disabledButton=false
                }
            }else if(newval=='0'){
                this.operateMultipleState=[]
                this.operperror=false
                this.opermessage=''
                this.disabledButton=false
            }
        },
        operateMultipleState(newval,oldval){
            if(this.multipleOperates=="1" && newval.length){
                this.opererror=false
                this.opermessage=''
                this.disabledButton=false
            }else if(this.multipleOperates=="1" && !newval.length){
                this.opererror=true
                this.opermessage='Required !'
                this.disabledButton=true
            }
        },
        line_coverage(newval,oldval){
            var error = false
                if(newval.indexOf(4)>-1 && (this.propertyEstimate==null||this.propertyEstimate=="")){
                    error = true
                    this.properror=true
                    this.propmessage='Required !'
                    this.disabledButton=true
                }else if(newval.indexOf(4)<=-1){
                    this.properror=false
                    this.propertyEstimate=null
                    this.propmessage=''
                    this.disabledButton=false
                }else{
                    this.properror=false
                    this.propmessage=''
                    this.disabledButton=false
                }
                if(newval.indexOf(5)>-1 && (this.umbrellaLimit==null||this.umbrellaLimit=="")){
                    error = true
                    this.umbrerror=true
                    this.umbrmessage='Required !'
                    this.disabledButton=true
                }
                else if(newval.indexOf(5)<=-1){
                    this.umbrerror=false
                    this.umbrellaLimit=null
                    this.umbrmessage=''
                    this.disabledButton=false
                }else{
                    this.umbrerror=false
                    this.umbrmessage=''
                    this.disabledButton=false
                }
                if(newval.indexOf(7)>-1 && (this.otherData==null||this.otherData=="")){
                    error = true
                    this.othererror=true
                    this.othermessage='Required !'
                    this.disabledButton=true
                }else if( newval.indexOf(7)<=-1){
                    this.othererror=false
                    this.otherData=null
                    this.othermessage=''
                    this.disabledButton=false
                }else{
                    this.othererror=false
                    this.othermessage=''
                    this.disabledButton=false
                }
            if(newval.length>0 && error ==false){
                this.disabledButton=false
            }else{
                this.disabledButton=true
            }
        },
        propertyEstimate(newval,oldval){	
            if(this.line_coverage.indexOf(4)>-1){
                if(newval==null||newval==""){
                    this.properror=true
                    this.propmessage='Required !'
                    this.disabledButton=true
                }else{
                    newval = newval.replace(/\D/g,'');
                    if(isNaN(newval)&&newval.length<=3){
                        this.propertyEstimate = null
                        this.properror=true
                        this.propmessage='Number Required !'
                        this.disabledButton=true
                    }else {
                        var datas = numeral(newval).format('$ 0,0[.]00')
                        this.propertyEstimate = datas
                        this.properror=false
                        this.propmessage=''
                        this.disabledButton=false
                    }
                }
            }else{
                // this.addValidation(4, 'add')
                this.propertyEstimate=null
            }
        },
        umbrellaLimit(newval,oldval){
            if(this.line_coverage.indexOf(5)>-1){
                if(newval==null||newval==""){
                    this.umbrerror=true
                    this.umbrmessage='Required !'
                    this.disabledButton=true
                }else{
                    newval = newval.replace(/\D/g,'');
                    if(isNaN(newval)){
                        this.umbrerror=true
                        this.umbrellaLimit = null
                        this.umbrmessage='Number Required !'
                        this.disabledButton=true
                    }else{
                        var datas = numeral(newval).format('$ 0,0[.]00')
                        this.umbrellaLimit = datas
                        this.umbrerror=false
                        this.umbrmessage=''
                        this.disabledButton=false
                    }
                }
            }else{
                this.umbrellaLimit=null
            }
        },
        otherData(newval,oldval){
            if(this.line_coverage.indexOf(7)>-1){
                if(newval==null||newval==""){
                    this.othererror=true
                    this.othermessage='Required !'
                    this.disabledButton=true
                }else{
                    if(!isNaN(newval)){
                        this.otherData = null
                        this.othererror=true
                        this.othermessage='Number Not Required !'
                        this.disabledButton=true
                    }else{
                        this.othererror=false
                        this.othermessage=''
                        this.disabledButton=false
                    }
                }
            }else{
                this.otherData=null
            }
        },
        annualCost(newval,oldval){
            if(newval==null||newval==""){
                this.annualerror=true
                this.annualmessage='Required !'
                this.disabledButton=true
            }else{
                newval = newval.replace(/\D/g,'');
                if(isNaN(newval)){
                    this.annualCost=null
                    this.annualerror=true
                    this.annualmessage='Number Required !'
                    this.disabledButton=true
                }else if((newval%1)!=0){
                    this.annualerror=true
                    this.annualmessage='Digit values not required !'
                    this.disabledButton=true
                }else{
                    var datas = numeral(newval).format('$ 0,0[.]00')
                    this.annualCost = datas
                    this.annualerror=false
                    this.annualmessage=''
                    this.disabledButton=false
                }
            }
        },
        annualPayroll(newval,oldval){
            if(newval==null||newval==""){
                this.annualperror=true
                this.annualpmessage='Required !'
                this.disabledButton=true
            }else{
                newval = newval.replace(/\D/g,'');
                if(isNaN(newval)){
                    this.annualPayroll=null
                    this.annualperror=true
                    this.annualpmessage='Number Required !'
                    this.disabledButton=true
                }else if((newval%1)!=0){
                    this.annualperror=true
                    this.annualpmessage='Digit value not required !'
                    this.disabledButton=true
                }else{
                    var datas = numeral(newval).format('$ 0,0[.]00')
                    this.annualPayroll = datas
                    this.annualperror=false
                    this.annualpmessage=''
                    this.disabledButton=false
                }
            }
        },
        rushRequests(newval,oldval){
            if(newval=="1"){
                if(this.coverageDate==null){
                    this.coverageerror=true
                    this.coveragemessage = 'Required !'
                    this.disabledButton=true
                }else{
                    this.coverageerror=false
                    this.coveragemessage = ''
                    this.disabledButton=false
                }
            }else if(newval=="0"){
                this.coverageerror=false
                this.coveragemessage = ''
                this.disabledButton=false
                this.coverageDate=null
            }
        },
        coverageDate(newval,oldval){
            if(this.rushRequests=="1"){
                if(newval && newval.length>0){
                    this.coverageerror=false
                    this.coveragemessage='',
                    this.disabledButton=false
                }else{
                    this.coverageerror=true
                    this.coveragemessage='Required !'
                    this.disabledButton=true
                }
            }else{
                // this.addValidation(16, 'add')
                this.coverageDate=null
            }
        },
        targetPremium(newval,oldval){
            if(newval=="1"){
                if(this.premiumTarget==null||this.premiumTarget==""){
                    this.premerror=true
                    this.premmessage = 'Required !'
                    this.disabledButton=true
                }else{
                    this.premerror=false
                    this.premmessage = ''
                    this.disabledButton=false
                }
            }else if(newval=="0"){
                this.premiumTarget=null
                this.premerror=false
                this.premmessage = ''
                this.disabledButton=false
            }
        },
        premiumTarget(newval,oldval){
            if(this.targetPremium=="1"){
                if(newval!=null){
                    newval = newval.replace(/\D/g,'');
                    if(isNaN(newval)){
                        this.premerror=true
                        this.premmessage='Number Required !',
                        this.disabledButton=false
                    }else{
                        var datas = numeral(newval).format('$ 0,0[.]00')
                        this.premiumTarget = datas
                        this.premerror=false
                        this.premmessage='',
                        this.disabledButton=false
                    }
                }else{
                    this.premerror=true
                    this.premmessage='Required !'
                    this.disabledButton=true
                }
            }else{
                this.premiumTarget=null
            }
        },
    }
}