import Vue from "vue";
import Router from "vue-router";
import store from "@/store";
import getMethods from "@/services/api/getMethods.js";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "NexaHome",
      component: () => import("@/views/NexaHome"),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          var userDetails = store.state.currentUserDetails;
          var data = { email_address: userDetails.email_address };
          getMethods.userEmailAvalibility(data).then(res => {
            if(!res){
              if (from.fullPath === "/") {
                if(userDetails && userDetails.user_type == 1){
                  if(userDetails.progress_status < 5){
                    next({ name: "getstarted", query: { route: "signup", user: "agent" } });
                  }else if( userDetails.progress_status == 5){
                    next({ name: "agentdashboardintro", query: { route: "direct", user: "agent" } });
                  } else if( userDetails.progress_status == 6){
                    next({ name: "agentdashboard", query: { route: "direct", user: "agent" } });
                  }
                }else if(userDetails && userDetails.user_type == 2){
                  if(userDetails.progress_status < 5){
                    next({ name: "getstarted", query: { route: "signup", user: "broker" } });
                  }else if( userDetails.progress_status == 5){
                    next({ name: "brokerdashboardintro", query: { route: "direct", user: "broker" } });
                  } else if( userDetails.progress_status == 6){
                    next({ name: "brokerdashboard", query: { route: "direct", user: "broker" } });
                  }
                }
              } else {
                next(false);
              }
            }else{
              store.commit('currentUserDetailsAdd',{})
              next();
            }
          });
        } else {
          next();
        }
      }
    },
    /// agent side ///
    {
      path: "/questionnaire/agent/",
      name: "agentquestionnaire",
      component: () =>
        import("@/components/NexaQuestionnaire/NexaAgentQuestionnaireContent")
    },
    {
      path: "/dashboard/agent/",
      name: "agentdashboard",
      component: () =>
        import(
          "@/components/NexaDashboard/NexaAgent/NexaAgentDashboardContent"
        ),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashboard/agent/intro",
      name: "agentdashboardintro",
      component: () =>
        import(
          "@/components/NexaDashboard/NexaAgent/NexaAgentDashboardContentIntro"
        ),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          var userDetails = store.state.currentUserDetails
          if(userDetails && userDetails.progress_status == 5){
            next();
          }else{
            next(false);
          }
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashboard/agent/createmarketsearch",
      name: "agentcreatemarketsearch",
      component: () =>
        import(
          "@/components/NexaDashboard/NexaAgent/NexaAgentCreateMarketSearch"
        ),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashboard/agent/edit/:key",
      name: "agentdahboardedit",
      component: () =>
        import(
          "@/components/NexaDashboard/NexaAgent/NexaAgentViewEditLeadData"
        ),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashbaord/agent/chat",
      name: "agentchatpage",
      component: () =>
        import(
          "@/components/NexaDashboard/NexaAgent/ChatNew/NexaAgentChatContent"
        ),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashboard/agent/chat",
      name: "agentdahboardchat",
      component: () =>
        import("@/components/NexaDashboard/NexaAgent/ChatNew/chat"),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },

    /// broker side ///
    {
      path: "/questionnaire/broker/",
      name: "brokerquestionnaire",
      component: () =>
        import("@/components/NexaQuestionnaire/NexaBrokerQuestionnaireContent")
    },
    {
      path: "/dashboard/broker/",
      name: "brokerdashboard",
      component: () =>
        import(
          "@/components/NexaDashboard/NexaBroker/NexaBrokerDashbardContent"
        ),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashboard/broker/intro",
      name: "brokerdashboardintro",
      component: () =>
        import(
          "@/components/NexaDashboard/NexaBroker/NexaBrokerDashboardContentForIntro"
        ),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          var userDetails = store.state.currentUserDetails
          if(userDetails && userDetails.progress_status == 5){
            console.log("inside if")
            next();
          }else{
            console.log("inside else")
            next(false);
          }
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashboard/broker/create-filter",
      name: "brokermarketfiltercreation",
      component: () =>
        import(
          "@/components/NexaDashboard/NexaBroker/NexaBrokerMarketFilterCreate"
        ),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashbaord/broker/chat",
      name: "brokerchatpage",
      component: () =>
        import(
          "@/components/NexaDashboard/NexaBroker/Chat/NexaBrokerChatContent"
        ),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashboard/broker/chat",
      name: "brokerdashboardchat",
      component: () =>
        import("@/components/NexaDashboard/NexaBroker/Chat/chat"),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },

    /// common section ///
    {
      path: "/dashboard/accounts/:location",
      name: "accounts",
      component: () => import("@/components/NexaDashboard/NexaAccountsContent"),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashboard/notifications",
      name: "notifications",
      component: () => import("@/components/NexaNotifications")
    },
    {
      path: "/getstarted",
      name: "getstarted",
      component: () =>
        import("@/components/NexaGetstarted/NexaGetstartedContent"),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          var userDetails = store.state.currentUserDetails
          if(userDetails && userDetails.progress_status < 5){
            next()
          }else if(userDetails && userDetails.progress_status == 5){
            if(userDetails.user_type == 1){
              next({ name: "agentdashboardintro", query: { route: "direct", user: "agent" } });
            } else if(userDetails.user_type == 2){
              next({ name: "brokerdashboardintro", query: { route: "direct", user: "broker" } });
            }
          }else if(userDetails && userDetails.progress_status ==6){
            if(userDetails.user_type == 1){
              next({ name: "agentdashboard", query: { route: "direct", user: "agent" } });
            } else if(userDetails.user_type == 2){
              next({ name: "brokerdashboard", query: { route: "direct", user: "broker" } });
            }
          }
        } else {
          next(false);
        }
      }
    },
    {
      path: "/invitation/signup/:id",
      name: "invitation",
      component: () =>
        import("@/components/NexaInvitedUsers/NexaInvitedUsersContent")
    },
    {
      path: "/callback",
      name: "callback",
      component: () => import("@/components/NexaCallback")
    },
    {
      path: "/forgot_password/reset/:id",
      name: "forgotpassword",
      component: () => import("@/components/NexaForgotPassword")
    },
    {
      path: "/forgot_password/verification/",
      name: "forgotpasswordverification",
      component: () => import("@/components/NexaForgotPasswordEmail")
    },
    {
      path: "/dashboard/myteam/:user",
      name: "myteam",
      component: () => import("@/components/NexaDashboard/TeamCreation/MyTeam"),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashboard/create_team/:user/:global_team_id",
      name: "createteam",
      component: () =>
        import("@/components/NexaDashboard/TeamCreation/TeamCreation"),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    {
      path: "/dashboard/:team_name/:user/:team_id/:global",
      name: "teamview",
      component: () =>
        import("@/components/NexaDashboard/TeamCreation/TeamView"),
      beforeEnter: (to, from, next) => {
        if (store.state.currentUserDetails.hasOwnProperty("user_type")) {
          next();
        } else {
          next(false);
        }
      }
    },
    { path: '*', component: () => import('@/components/NexaPageNotFound') }
  ]
});
