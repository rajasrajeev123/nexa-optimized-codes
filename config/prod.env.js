"use strict";
/* module.exports = {
  NODE_ENV: '"production"',
  BASE_URL: '"/Nexa"',
  VUE_BACKEND_URL: '"http://3.121.72.154/backend/public/api/"',
  VUE_IMAGE_URL:
    '"http://3.121.72.154/backend/public/uploads/users/file_uploads/"',
  VUE_FILE_URL:
    '"http://3.121.72.154/backend/public/uploads/users/market_list/"',
  VUE_CHAT_FILE: '"http://3.121.72.154/backend/public/uploads/users/chats/"',
  VUE_PUSH_ICON_URL: '"http://3.121.72.154/nexa-html/img/"',
  // VUE_BACKEND_URL: '"https://localhost/nexa/public/api/"',
  // VUE_IMAGE_URL: '"https://localhost/nexa/public/uploads/users/file_uploads/"',
  VUE_APP_LINKED_IN_ID: '"814prhv08zwhfk"',
  VUE_APP_LINKED_IN_REDIRECT_URL: '"/callback"',
  VUE_APP_PUBNUB_PUBLISH_KEY: '"pub-c-f02f3ac2-2045-47d8-bb5b-370e4f2eb391"',
  VUE_APP_PUBNUB_SUBSCRIBE_KEY: '"sub-c-07a04ca0-f932-11e8-80f1-b6259b5c8742"',
  VUE_APP_PUBNUB_SECERET_KEY:
    '"sec-c-YTMyZDY3MGYtNGM3Mi00NWRhLWE0OTQtNDY3MTVmN2E0MDgx"',
  VUE_CHAT_BACKEND: '"http://3.121.72.154:3001"',
  VUE_NEW_USER_VIDEO_URL: '"http://3.121.72.154/backend/public/video/"',
  VUE_STRIPE_PUBLISH_KEY: '"pk_test_qYTwUUUXW5RP5pCCYCjdfN0L"',
  VUE_STRIPE_SECRET_KEY: '"sk_test_sIq1LBfXnreJkZsSLexjchD4"',
  VUE_APP_PUBNUB_UPLOADCARE_PUBLIC_KEY: '"30287b8f999976c51d74"',
  VUE_APP_PUBNUB_UPLOADCARE_SCERET_KEY: '"f724e28e880d2f4ee735"',
  VUE_PUSHER_APP_ID: '"666172"',
  VUE_PUSHER_CLUSTER: '"us2"',
  VUE_PUSHER_KEY: '"db56bbe216e6db1fc38a"',
  VUE_PUSHER_SECRET: '"2b9d22533c812466584e"',
  VUE_URL: '"http://3.121.72.154"'
}; */
module.exports = {
  NODE_ENV: '"production"',
  BASE_URL: '"/Nexa"',
  VUE_BACKEND_URL: '"https://nexa.solutions/backend/public/api/"',
  VUE_IMAGE_URL:
    '"https://nexa.solutions/backend/public/uploads/users/file_uploads/"',
  VUE_FILE_URL:
    '"https://nexa.solutions/backend/public/uploads/users/market_list/"',
  VUE_CHAT_FILE: '"https://nexa.solutions/backend/public/uploads/users/chats/"',
  VUE_PUSH_ICON_URL: '"https://nexa.solutions/nexa-html/img/"',
  // VUE_BACKEND_URL: '"http://localhost/nexa/public/api/"',
  // VUE_IMAGE_URL: '"http://localhost/nexa/public/uploads/users/file_uploads/"',
  VUE_APP_LINKED_IN_ID: '"78x8ts7xmbvbbn"',
  VUE_APP_LINKED_IN_REDIRECT_URL: '"/callback"',
  VUE_APP_PUBNUB_PUBLISH_KEY: '"pub-c-f02f3ac2-2045-47d8-bb5b-370e4f2eb391"',
  VUE_APP_PUBNUB_SUBSCRIBE_KEY: '"sub-c-07a04ca0-f932-11e8-80f1-b6259b5c8742"',
  VUE_APP_PUBNUB_SECERET_KEY:
    '"sec-c-YTMyZDY3MGYtNGM3Mi00NWRhLWE0OTQtNDY3MTVmN2E0MDgx"',
  VUE_CHAT_BACKEND: '"https://nexa.solutions/:3001"',
  VUE_NEW_USER_VIDEO_URL: '"https://nexa.solutions/backend/public/video/"',
  VUE_STRIPE_PUBLISH_KEY: '"pk_test_qYTwUUUXW5RP5pCCYCjdfN0L"',
  VUE_STRIPE_SECRET_KEY: '"sk_test_sIq1LBfXnreJkZsSLexjchD4"',
  VUE_APP_PUBNUB_UPLOADCARE_PUBLIC_KEY: '"30287b8f999976c51d74"',
  VUE_APP_PUBNUB_UPLOADCARE_SCERET_KEY: '"f724e28e880d2f4ee735"',
  VUE_PUSHER_APP_ID: '"666170"',
  VUE_PUSHER_CLUSTER: '"us2"',
  VUE_PUSHER_KEY: '"5cdea5816bcd808bca14"',
  VUE_PUSHER_SECRET: '"87ca53bc36160688e3b3"',
  VUE_URL: '"https://nexa.solutions"'
};
