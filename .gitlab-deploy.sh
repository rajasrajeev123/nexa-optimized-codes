#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVER
array=(${string//,/ })
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"
do      
    echo "Deploying to server ${array[i]}"
    # ssh ubuntu@${array[i]} "cd ~/nexa-optimized-codes && git pull && npm run build && sudo cp -R dist/ /var/www/html && cd /var/www/html/backend && git pull origin master"
    ssh ubuntu@${array[i]} "cd ~/nexa-optimized-codes && git pull && cd /var/www/html && git pull"
done